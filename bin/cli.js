#!/usr/bin/env node
const
  hyperlinker = require(`hyperlinker`),
  { Signale } = require(`signale`),
  supportsHyperslinks = require(`supports-hyperlinks`),
  yargs = require(`yargs`);

const
  OrpinBootstrap = require(`../lib/orpin-bootstrap.js`),
  OrpinError = require(`../lib/orpin-errors.js`),
  OrpinOpts = require(`../lib/orpin-options.js`),
  Orpin = require(`../lib/orpin.js`);

const
  pkgVersion = require(`../package.json`).version,
  signaler = new Signale({interactive: true});

let maybeHyperlink = `https://gitlab.com/vaemoi/orpin/orpin-cli`;

if (supportsHyperslinks.stdout) {
  maybeHyperlink = hyperlinker(`here`, maybeHyperlink);
}

const flags = yargs
  .options(OrpinOpts)
  .alias(`h`, `help`)
  .alias(`v`, `version`)
  .usage(`\nUsage:\n\n    $0 [<options>]`)
  .help(`help`)
  .version(pkgVersion)
  .epilogue(`for more information, checkout ${maybeHyperlink}`)
  .wrap(yargs.terminalWidth() - 10)
  .argv;

const devMode = flags.force_download || !process.env.PRODUCTION;

let config;
try {
  signaler.await(`Checking config`);
  config = OrpinBootstrap.go(flags.config_path, devMode);

  if (config.errors.length > 0) {
    throw new OrpinError.BaseError(`Encountered error(s) starting orpin:`);
  }

  if (flags.check) {
    signaler.success(`Orpin is good to go.`);
  } else {
    Orpin(config, flags.defer_off, devMode, signaler);
  }
} catch (err) {
  signaler.fatal(`orpin encountered problems:\n`);
  OrpinError.handle(err, signaler);
}

