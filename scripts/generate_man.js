const
  fs = require(`fs`),
  path = require(`path`),
  remark = require(`remark`),
  man = require(`remark-man`),
  vfile = require(`to-vfile`);

const
  readme = path.resolve(__dirname, `../README-man.md`),
  outputPath = path.resolve(__dirname, `../man`);

fs.mkdirSync(outputPath, {recursive: true});

remark()
  .use(man)
  .process(vfile.readSync(readme), (err, file) => {
    if (err) {
      throw err;
    }

    const contents = file.toString();

    fs.writeFileSync(path.join(outputPath, `orpin-cli.doc.1`), contents);
  });
