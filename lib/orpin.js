const
  OrpinDOM = require(`./orpin-dom.js`),
  OrpinError = require(`./orpin-errors.js`),
  OrpinFetch = require(`./orpin-fetch.js`);

/**
 * Main function for orpin
 *
 * @param      {Object}   config      Necessary runtime info
 * @param      {boolean}  deferOff    Whether or not to add 'defer' to injected scripts
 * @param      {boolean}  devMode  Whether or not do download fetched files
 * @param      {Object}   signaler The reporter to use for logging
 *
 */
const Orpin = async (config, deferOff, devMode, signaler) => {
  try {
    signaler.info(`Starting orpin`);

    const { anchor, indexDOM, indexPath, pkgs} = config;
    signaler.success(`Checked config`);

    await OrpinFetch.remote(pkgs);
    signaler.success(`Fetched remote files`);

    if (devMode) {
      await OrpinFetch.persist(pkgs);
      signaler.success(`Saved remote files`);
    }

    const finalDOM = OrpinDOM.scripts(anchor, deferOff, devMode, indexDOM, pkgs);
    signaler.success(`Injected dependencies`);

    OrpinDOM.writeIfDiff(indexPath, indexDOM, finalDOM);
    signaler.success(`orpin finished successfully`);
  } catch (err) {
    signaler.fatal(`orpin encountered problems:\n`);
    OrpinError.handle(err, signaler);
  }
};

module.exports = Orpin;
