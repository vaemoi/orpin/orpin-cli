const Verror = require(`@vaemoi/verrors-node`);

const OrpinWard = require(`./orpin-ward.js`);

class BaseError extends Verror {}

class ValidationError extends BaseError {}

class MissingConfigError extends ValidationError {
  constructor(key) {
    super(`Missing ${key} in orpin config`);
  }
}

class MissingConfigValueError extends MissingConfigError {
  constructor(key) {
    super(`value for ${key}`);
  }
}

class MissingPackageVersionError extends MissingConfigError {
  constructor(packageName) {
    super(`version from package.json for ${packageName}`);
  }
}

class MissingPackagePathError extends MissingConfigError {
  constructor(packageName) {
    super(`path or devPath for ${packageName}`);
  }
}

class ReadError extends ValidationError {
  constructor(fileName, filePath) {
    super(`Path to ${fileName} -- doesn't exist\n\t-> ${filePath}`);
  }
}

class NoAccessError extends ValidationError {
  constructor(value, valuePath) {
    super(`Invalid permissions on path for ${value}\n\t-> ${valuePath}`);
  }
}

class FetchError extends BaseError {
  constructor(message, code, url) {
    super(`Problem with request -- ${message}: ${code}\n\t${url}`);
  }
}

class BadURLError extends FetchError {
  constructor(code, url) {
    super(`probably a bad url`, code, url);
  }
}

class ServerError extends FetchError {
  constructor(code, url) {
    super(`server error`, code, url);
  }
}

const withErrorCapture = (func) => {
  return async function (...args) {
    let result = {value: null, errors: []};

    try {
      result = await func(...args);
    } catch (err) {
      OrpinWard.captureException(err);
      result.errors.unshift(err);
    }

    return result;
  };
};

const handleError = (err, signaler) => {
  if (err instanceof BaseError) {
    signaler.error(err.display());
  } else {
    signaler.error(err);
  }
};

// Exports
module.exports = {
  BadURL: BadURLError,
  Base: BaseError,
  handle: handleError,
  MissingConfig: MissingConfigError,
  MissingConfigValue: MissingConfigValueError,
  MissingPackagePath: MissingPackagePathError,
  MissingPackageVersion: MissingPackageVersionError,
  NoAccess: NoAccessError,
  Read: ReadError,
  Response: FetchError,
  Server: ServerError,
  Validation: ValidationError,
  CaptureDecorator: withErrorCapture
};
