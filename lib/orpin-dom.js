const
  cheerio = require(`cheerio`),
  fs = require(`fs`),
  hiff = require(`hiff`);

/**
 * Write DOM out to file if contents have changed
 * @param      {string}   indexPath
 * @param      {Object}   baseDOM
 * @param      {Object}   finalDOM
 */
const maybeWrite = (indexPath, baseDOM, finalDOM) => {
  const result = hiff.compare(baseDOM.html(), finalDOM.html());

  if (result.different) {
    fs.writeFileSync(indexPath, finalDOM.html());
  }
};

/**
 * Add <script/> tags to a DOM before a specific node
 *
 * @param      {string}   anchor    cheerio selector compatible string for the element that will come after the inejcted tag
 * @param      {boolean}  deferOff  Whether or not to inject a defer attribute
 * @param      {boolean}  devMode   Whether or not to leave out the crossorigin attribute
 * @param      {string}   dom       Wrapper for DOM we'll inject scripts into
 * @param      {Object}   pkgs      List of infos containing urls to inject
 *
 * @return     {Object}   An updated domObj with <script/> tags injected
 */
const injectScripts = (anchor, deferOff, devMode, dom, pkgs) => {
  const domClone = cheerio.load(dom.html());

  for (const pkg of pkgs) {
    const srcURI = pkg.srcURI;

    domClone(`script[src='${srcURI}']`).remove();

    const
      crossOrigin = devMode ? `` : `crossorigin`,
      defer = deferOff ? `` : `defer`;

    // @todo       Add flag for combining into single url
    domClone(`<script ${crossOrigin} ${defer} src="${srcURI}"></script>`).insertBefore(anchor);
  }

  return domClone;
};

module.exports = {
  scripts: injectScripts,
  writeIfDiff: maybeWrite
};
