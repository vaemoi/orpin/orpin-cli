const
  tedo = require(`testdouble`),
  _tapePromise = require(`tape-promise`).default,
  tape = require(`tape`);

const test = _tapePromise(tape);

const OrpinError = require(`../orpin-errors.js`);

test(`Should handle errors`, (swear) => {
  const mockSignaler = {
    await: () => {},
    error: () => {},
    fatal: () => {},
    info: () => {},
    pending: () => {},
    success: () => {},
    warn: () => {}
  };

  swear.plan(2);

  const
    mcErr = new OrpinError.MissingConfig(`some_key`),
    someErr = new Error();

  swear.ok(OrpinError.handle(mcErr, mockSignaler) === undefined, `Should handle OrpinErrors`);
  swear.ok(OrpinError.handle(someErr, mockSignaler) === undefined, `Should handle non OrpinErrors`);
});

test(`Should capture errors in execution`, async (swear) => {
  // Mock the OrpinWard call so we don't send anything to Sentry during testing
  const orpinward = tedo.replace(`../orpin-ward.js`, { /* eslint-disable-line no-unused-vars */
    captureException: tedo.func(() => { console.log(`called captureException`); })
  });

  const
    errMsg = `haha neenenenee booboo`,
    theError = new Error(errMsg);

  const
    badFunc = OrpinError.CaptureDecorator(() => { throw theError; }),
    goodFunc = OrpinError.CaptureDecorator((numX, numY) => { return numX + numY; });

  swear.plan(2);

  const
    badResult = await badFunc(),
    goodResult = await goodFunc(1, 1);


  swear.deepEqual(badResult, {value: null, errors: [theError]}, `Returns expected value from an erroneous func call`);
  swear.equal(goodResult, 2, `Returns expected value from a non erroneous func call`);

  tedo.reset();
});
