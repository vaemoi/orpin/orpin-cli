const
  fs = require(`fs`),
  path = require(`path`),
  rimraf = require(`rimraf`),
  _tapePromise = require(`tape-promise`).default,
  tape = require(`tape`);

const test = _tapePromise(tape);

const Orpin = require(`../orpin.js`);

// Collect a list of files and/or dirs to remove after testing is done.
const tidyPaths = [];
const resetPaths = [];

// Anything that needs to be done after all tests are finished or failed
const teardown = () => {
  tidyPaths.forEach((tPath) => {
    rimraf.sync(tPath);
  });

  resetPaths.forEach((rPath) => {
    fs.writeFileSync(rPath[1], fs.readFileSync(rPath[0]));
  });
};

test(`Should test orpin main function`, async (swear) => {
  const mockSignaler = {
    await: () => {},
    error: () => {},
    fatal: () => {},
    info: () => {},
    pending: () => {},
    success: () => {},
    warn: (msg) => { throw new Error(msg); }
  };

  const
    aokDeferOff = false,
    aokDevMode = true,
    aokPath = path.join(__dirname, `./fixtures/example`),
    aokVendorPath = path.join(aokPath, `public/assets/vendor`);

  const aokConfig = {
    anchor: `script[src='js/something.js']`,
    indexDOM: null,
    indexPath: path.join(aokPath, `public/index.html`),
    pkgs: null
  };

  tidyPaths.push(aokVendorPath);
  resetPaths.push([
    path.join(aokPath, `public/index.html.og`),
    path.join(aokPath, `public/index.html`)
  ]);

  const fileCountBefore = 0;

  swear.plan(4);

  swear.notOk(fs.existsSync(aokVendorPath), `No vendor path before`);

  await Orpin(aokConfig, aokDeferOff, !aokDevMode, mockSignaler);

  let fileCountAfter = (await fs.promises.readdir(aokVendorPath)).length;

  swear.ok(fileCountAfter === fileCountBefore, `Downloads nothing when not in devMode`);

  await Orpin(aokConfig, aokDeferOff, aokDevMode, mockSignaler);

  fileCountAfter = (await fs.promises.readdir(aokVendorPath)).length;

  swear.ok(fs.existsSync(aokVendorPath), `Creates vendor path`);
  swear.ok(fileCountAfter > fileCountBefore && fileCountAfter === 2, `Fetches expected number of packages`);
});

test(`Handles errors 'gracefully'`, async (swear) => {
  const mockSignaler = {
    await: () => {},
    error: () => {},
    fatal: () => {},
    info: () => {},
    pending: () => {},
    success: () => {},
    warn: () => {}
  };

  swear.plan(1);

  await Orpin(null, null, null, mockSignaler);

  swear.ok(true, `Doesn't throw any errors just displays them`);

});

test.onFinish(teardown);
test.onFailure(teardown);
