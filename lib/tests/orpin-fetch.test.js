const
  fs = require(`fs`),
  path = require(`path`),
  rimraf = require(`rimraf`),
  _tapePromise = require(`tape-promise`).default,
  tape = require(`tape`);

const test = _tapePromise(tape);

const
  OrpinError = require(`../orpin-errors.js`),
  OrpinFetch = require(`../orpin-fetch.js`);

// Collect a list of files and/or dirs to remove after testing is done.
const tidyPaths = [];

// Anything that needs to be done after all tests are finished or failed
const teardown = () => {
  tidyPaths.forEach((tPath) => {
    rimraf.sync(tPath);
  });
};

test(`Should fetch package files from cdn`, async (swear) => {
  const infos = [
    {
      cdnURI: `https://cdn.jsdelivr.net/npm/dompurify@1.0.11/dist/purify.js`,
      blob: null
    },
    {
      cdnURI: `https://cdn.jsdelivr.net/npm/localforage@1.7.3/dist/localforage.min.js`,
      blob: null
    }
  ];

  const countInfosReducer = (count, info) => {
    if (info.blob && typeof info.blob === `string` && info.blob.length > 0) {
      count += 1;
    }

    return count;
  };

  const blobsBefore = infos.reduce(countInfosReducer, 0);

  swear.plan(1);

  try {
    await OrpinFetch.remote(infos);

    const blobsAfter = infos.reduce(countInfosReducer, 0);

    swear.ok(blobsBefore !==  blobsAfter && blobsAfter === 2, `Fetch expected number of files`);
  } catch (err) {
    if (err instanceof OrpinError.Base) {
      err.display();
    } else {
      console.error(err);
    }

    swear.end();
  }
});

test(`Should throw errors when fetching package files from cdn`, async (swear) => {
  const infos = [
    {
      cdnURI: `https://cdn.jsdelivr.net/npm/dompurify@1.0.11/dist/localforage.js`,
      blob: null
    },
    {
      cdnURI: `https://cdn.jsdelivr.net/npm/localforage@1.7.3/dist/localforage.min.js`,
      blob: null
    }
  ];

  swear.plan(3);

  try {
    await OrpinFetch.remote(infos);
  } catch (err) {
    swear.ok(err instanceof OrpinError.BadURL, `Should throw a server response error for 4xx response codes`);
  }

  const infosDos = [{
    cdnURI: `https://httpstat.us/503`,
    blob: null
  }];

  try {
    await OrpinFetch.remote(infosDos);
  } catch (err) {
    const maybeErrorMessage = `Problem with request -- server error: 503\n\thttps://httpstat.us/503`;

    swear.equal(err.missive, maybeErrorMessage, `Spits out correct error message`);
    swear.ok(err instanceof OrpinError.Server, `Should throw a server response error for 5xx response codes`);
  }
});

test(`Write files from package info`, async (swear) => {
  const
    fixturePath = path.join(__dirname, `./fixtures/fetch_example/public/assets/vendor`),
    infos = [
      {
        localURI: `${fixturePath}/somepkg/1.0.0/file.js`,
        blob: `The file contents are bleak.`,
        name: `SomePkg`
      }
    ];

  const fileCountBefore = (await fs.promises.readdir(fixturePath)).length;

  infos.forEach((info) => {
    tidyPaths.push(path.dirname(path.dirname(info.localURI)));
  });

  swear.plan(1);

  await OrpinFetch.persist(infos);

  const fileCountAfter = (await fs.promises.readdir(fixturePath)).length;

  swear.ok(fileCountAfter !== fileCountBefore && fileCountAfter === 2, `Adds expected number of files to vendorPath`);
});

test.onFinish(teardown);
test.onFailure(teardown);
