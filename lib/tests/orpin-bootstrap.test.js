const
  fs = require(`fs`),
  path = require(`path`),
  rimraf = require(`rimraf`),
  test = require(`tape`);

const
  OrpinBootstrap = require(`../orpin-bootstrap.js`),
  OrpinError = require(`../orpin-errors.js`);

// Spits out valid meta info to compare against
const metaFixture = (fixturePath) => {
  return {
    packageJSONPath: path.join(fixturePath, `package.json`),
    config: {
      "anchorSelector": `script[src='js/something.js']`,
      "indexFile": `public/index.html`,
      "output": {
        "path": `public/assets/vendor`,
        "srcPrefix": `assets/vendor`
      },
      "packages": [
        {
          "name": `dompurify`,
          "path": `dist/purify.js`
        },
        {
          "name": `localforage`,
          "devPath": `dist/localforage.js`,
          "path": `dist/localforage.min.js`
        },
        {
          "name": `lodash.debounce`,
          "path": `dist/lodash.debounce.min.js`,
          "skip": true
        }
      ]
    },
    indexPath: path.join(fixturePath, `public/index.html`),
    vendorPath: path.join(fixturePath, `public/assets/vendor`)
  };
};

// Creates a function that will throw an error when called with supplied args
const errorSpewer = (caller, args) => {
  return () => {
    caller(...args);
  };
};

// Collect a list of files and/or dirs to remove after testing is done.
const tidyPaths = [];

// Anything that needs to be done after all tests are finished or failed
const teardown = () => {
  tidyPaths.forEach((tPath) => {
    rimraf.sync(tPath);
  });

  try {
    fs.chmodSync(path.join(__dirname, `./fixtures/bootstrap_example/badOutputPath/public/assets/vendor`), 0o755);
  } catch (err) {
    console.log(`Can't change permissions`);
  }
};

test(`Should return meta info from a valid .orpinrc file`, (swear) => {
  const
    aokPath = path.resolve(__dirname, `./fixtures/bootstrap_example/aok`),
    aokMeta = metaFixture(aokPath);

  tidyPaths.push(aokMeta.vendorPath);

  swear.plan(7);

  swear.notOk(fs.existsSync(aokMeta.vendorPath), `No vendor path before`);

  const maybeMeta = OrpinBootstrap.meta(path.join(aokPath, `.orpinrc`));

  swear.deepEqual(maybeMeta.config, aokMeta.config, `read .orpinrc`);
  swear.equal(maybeMeta.packageJSONPath, aokMeta.packageJSONPath, `read package.json`);
  swear.equal(maybeMeta.indexPath, aokMeta.indexPath, `read indexFile path`);
  swear.equal(maybeMeta.vendorPath, aokMeta.vendorPath, `read vendorPath`);
  swear.ok(fs.existsSync(aokMeta.vendorPath), `Creates vendor path when it doesn't exist`);

  const maybeMetaDos = OrpinBootstrap.meta(aokPath);

  swear.ok(maybeMetaDos, `read .orpinrc if given a directory for config path`);
});

test(`Should throw error when passed a bad config path`, (swear) => {
  swear.plan(1);

  swear.throws(OrpinBootstrap.meta, OrpinError.Read, `Throws read error when unable to find .orpinrc`);
});

test(`Should throw error when passed yucky config file`, (swear) => {
  const badIndexFileSpewer = errorSpewer(
    OrpinBootstrap.meta,
    [path.join(__dirname, `./fixtures/bootstrap_example/badIndexFile`)]
  );
  const noIndexFileSpewer = errorSpewer(
    OrpinBootstrap.meta,
    [path.join(__dirname, `./fixtures/bootstrap_example/noIndexFile`)]
  );
  const badOutputPathSpewer = errorSpewer(
    OrpinBootstrap.meta,
    [path.join(__dirname, `./fixtures/bootstrap_example/badOutputPath`)]
  );
  const noOutputPathSpewer = errorSpewer(
    OrpinBootstrap.meta,
    [path.join(__dirname, `./fixtures/bootstrap_example/noOutputPath`)]
  );
  const noOutputSrcPrefixSpewer = errorSpewer(
    OrpinBootstrap.meta,
    [path.join(__dirname, `./fixtures/bootstrap_example/noOutputSrcPrefix`)]
  );
  const noAnchorSelectorSpewer = errorSpewer(
    OrpinBootstrap.meta,
    [path.join(__dirname, `./fixtures/bootstrap_example/noAnchorSelector`)]
  );
  const badAnchorSelectorSpewer = errorSpewer(
    OrpinBootstrap.meta,
    [path.join(__dirname, `./fixtures/bootstrap_example/badAnchorSelector`)]
  );
  const noPackageJSONSpewer = errorSpewer(
    OrpinBootstrap.meta,
    [path.join(__dirname, `./fixtures/bootstrap_example/noPackageJSON`)]
  );

  swear.plan(8);

  swear.throws(badIndexFileSpewer, OrpinError.Read, `Throws read error when unable to find indexFile`);
  swear.throws(noIndexFileSpewer, OrpinError.MissingConfigValue, `Throws missing config error when unable to find indexFile in .orpinrc`);
  swear.throws(noOutputPathSpewer, OrpinError.MissingConfigValue, `Throws missing config error when unable to find output.path in .orpinrc`);
  swear.throws(noOutputSrcPrefixSpewer, OrpinError.MissingConfigValue, `Throws missing config error when no output.srcPrefix in .orpinrc`);
  swear.throws(noAnchorSelectorSpewer, OrpinError.MissingConfigValue, `Throws missing config error when no anchorSelector .orpinrc`);
  swear.throws(badAnchorSelectorSpewer, OrpinError.ValidationError, `Throws validation error is anchorSelector can't be found within the DOM from indexFile`);
  swear.throws(noPackageJSONSpewer, OrpinError.Read, `Throws read error when unable to find a package.json in the same dir as .orpinrc`);

  try {
    fs.chmodSync(path.join(__dirname, `./fixtures/bootstrap_example/badOutputPath/public/assets/vendor`), 0o444);
    swear.throws(badOutputPathSpewer, OrpinError.NoAccess, `Throws no access error when unable to read indexFile`);
  } catch (err) {
    console.log(`Can't change permissions`);
    swear.skip();
  }

});

test(`Should build packages`, (swear) => {
  const aokPath = path.join(__dirname, `./fixtures/bootstrap_example/aok`);

  const {
    config,
    packageJSONPath,
    vendorPath
  } = metaFixture(aokPath);

  const packageJSON = require(packageJSONPath);

  swear.plan(1);

  const maybePkgs = OrpinBootstrap.packages(
    config, false, packageJSON.dependencies, vendorPath
  );

  swear.ok(maybePkgs.length === 2, `Builds expected number of packages`);
});

test(`Should throw error when package.json isn't valid`, (swear) => {
  const missingPkgVersionPath = path.join(__dirname, `./fixtures/bootstrap_example/missingPkgVersion`);
  const { config, packageJSONPath, vendorPath} = metaFixture(missingPkgVersionPath);
  const packageJSON = require(packageJSONPath);

  const missingPkgVersion = errorSpewer(
    OrpinBootstrap.packages,
    [config, false, packageJSON.dependencies, vendorPath]
  );

  swear.plan(1);
  swear.throws(missingPkgVersion, OrpinError.MissingPackageVersion, `Throws missing version error when a packge isn't listed in package.json['dependencies']`);
});

test(`Should bootstrap successfully`, (swear) => {
  const aokPath = path.join(__dirname, `./fixtures/bootstrap_example/aok`);
  swear.plan(2);

  const maybeCore = OrpinBootstrap.go(aokPath, true);

  swear.ok(maybeCore, `Returns a valid core after bootstrapping`);

  const bootstrapErrorSpewer = errorSpewer(
    OrpinBootstrap.go,
    [null, true]
  );

  swear.throws(bootstrapErrorSpewer, OrpinError.Read, `Throws an errors if bootstrapping with a bad config path`);
});

test.onFinish(teardown);
test.onFailure(teardown);
