const
  cheerio = require(`cheerio`),
  fs = require(`fs`),
  hiff = require(`hiff`),
  rimraf = require(`rimraf`),
  path = require(`path`),
  test = require(`tape`);

const OrpinDOM = require(`../orpin-dom.js`);

const originalHTML = `
  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>Something JS</title>
      <script src="js/something.js"></script>
    </head>
    <body>
      <h1>Hola</h1>
    </body>
  </html>
`;

const tidyPaths = [];

// Anything that needs to be done after all tests are finished or failed
const teardown = () => {
  tidyPaths.forEach((tPath) => {
    rimraf.sync(tPath);
  });
};

test(`Should inject script tags`, (swear) => {
  const
    anchor = `script[src="js/something.js"]`,
    devMode = true,
    deferOff = false,
    dom = cheerio.load(originalHTML),
    pkgs = [
      {srcURI: `https://somevalidsite.net/path/to/file.js`},
      {srcURI: `https://othervalidsite.org/path/to/thing.js`}
    ];

  swear.plan(6);

  const dirtyDOM = OrpinDOM.scripts(anchor, deferOff, devMode, dom, pkgs);

  const inOrder = dirtyDOM(
    `script[src="https://somevalidsite.net/path/to/file.js"] + script[src="https://othervalidsite.org/path/to/thing.js"] + script[src="js/something.js"]`
  ).length === 1;

  swear.ok(hiff.compare(dom.html(), dirtyDOM.html()).different, `Updates the DOM`);
  swear.ok(inOrder, `Maintains insertion order`);
  swear.ok(dirtyDOM(`script[src="https://somevalidsite.net/path/to/file.js"]`).attr(`defer`), `Adds the defer attribute`);
  swear.notOk(dirtyDOM(`script[src="https://somevalidsite.net/path/to/file.js"]`).attr(`crossorigin`), `Doesn't add the crossorigin attribute`);

  const dirtyDOMDos = OrpinDOM.scripts(anchor, !deferOff, !devMode, dom, pkgs);

  swear.notOk(dirtyDOMDos(`script[src="https://somevalidsite.net/path/to/file.js"]`).attr(`defer`), `Doesn't add defer attribute`);
  swear.ok(dirtyDOMDos(`script[src="https://somevalidsite.net/path/to/file.js"]`).attr(`crossorigin`) === ``, `adds crossorigin attribute`);
});

test(`Should write files`, (swear) => {
  const
    originalDOM = cheerio.load(originalHTML),
    originalDOMClone = cheerio.load(originalDOM.html()),
    dirtyDOM = cheerio.load(originalDOM.html()),
    writePath = path.join(__dirname, `./fixtures/dom_example/index.html`);

  tidyPaths.push(writePath);

  swear.plan(2);

  OrpinDOM.writeIfDiff(writePath, originalDOM, originalDOMClone);
  swear.notOk(fs.existsSync(writePath), `Should skip writing when no diff in html DOM`);

  dirtyDOM(`body`).append(`<h2>I'm dirty DOM</h2>`);
  OrpinDOM.writeIfDiff(writePath, originalDOM, dirtyDOM);
  swear.ok(fs.existsSync(writePath), `Should write when diff is present in html DOM`);
});

test.onFinish(teardown);
test.onFailure(teardown);
