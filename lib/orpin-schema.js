const
  Joi = require(`@hapi/joi`),
  CSI = require(`css-selector-inspector`).default,
  validPath = require(`valid-path`);

const OrpinError = require(`./orpin-errors.js`);

const OrpinJoi = Joi.extend((joi) => {
  return {
    base: joi.string(),
    name: `string`,
    language: {
      domSelector: `needs to be a valid dom/css selector`,
      path: `needs to be a valid file path on linux/bsd/windows systems`
    },
    rules: [
      {
        name: `domSelector`,
        setup(params) { /* eslint-disable-line no-unused-vars */
          this._flags.domSelector = true;
        },
        validate(params, value, state, options) {
          if (!CSI.isValid(value)) {
            return this.createError(`string.domSelector`, { v: value }, state, options);
          }

          return value;
        }
      },
      {
        name: `path`,
        setup(params) { /* eslint-disable-line no-unused-vars */
          this._flags.path = true;
        },
        validate(params, value, state, options) {
          if (!validPath(value)) {
            return this.createError(`string.path`, { v: value }, state, options);
          }

          return value;
        }
      }
    ]
  };
});

const OrpinSchema = OrpinJoi.object({
  anchorSelector: OrpinJoi.string().domSelector().required().error(new OrpinError.MissingConfigValue(`anchorSelector`)),
  indexFile: OrpinJoi.string().path().required().error(new OrpinError.MissingConfigValue(`indexFile`)),
  output: OrpinJoi.object({
    path: OrpinJoi.string().path().required().error(new OrpinError.MissingConfigValue(`output.path`)),
    srcPrefix: OrpinJoi.string().uri({allowRelative: true}).required().error(new OrpinError.MissingConfigValue(`output.srcPrefix`))
  }),
  packages: OrpinJoi.array().items(OrpinJoi.object().keys({
    devPath: OrpinJoi.string().path(),
    name: OrpinJoi.string().required(),
    path: OrpinJoi.string().path().required().error(new OrpinError.MissingPackagePath(`\b`)),
    skip: OrpinJoi.boolean()
  }))
});

module.exports = OrpinSchema;
