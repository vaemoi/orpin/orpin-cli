const
  fetch = require(`node-fetch`), // TODO maybe: replace with 'http'
  fs = require(`fs`),
  path = require(`path`);

const OrpinError = require(`./orpin-errors.js`);

/**
 * Fetches remotes.
 *
 * @param      {Array<Object>}  infos   Package data needed to construct the urls & paths
 */
const fetchRemotes = async (infos) => {
  for (const info of infos) {
    const response = await fetch(info.cdnURI);

    if (!response.ok) {
      const err = response.status < 500 ? OrpinError.BadURL : OrpinError.Server;

      throw new err(response.status, info.cdnURI);
    }

    info.blob = await response.text();
  }
};

/**
 * Saves fetches.
 *
 * @param      {Array<Object>}  infos   Package data needed to construct the urls & paths
 */
const saveFetches = async (infos) => {
  for (const info of infos) {
    await fs.promises.mkdir(path.dirname(info.localURI), {recursive: true});
    await fs.promises.writeFile(info.localURI, info.blob);
    await fs.promises.access(info.localURI, fs.constants.F_OK | fs.constants.W_OK);
  }
};

module.exports = {
  remote: fetchRemotes,
  persist: saveFetches
};
