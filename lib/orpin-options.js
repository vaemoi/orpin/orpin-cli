module.exports = {
  c: {
    alias: `config_path`,
    default: `./.orpinrc`,
    describe: `Path to .orpinrc`,
    type: `string`
  },
  d: {
    alias: `defer_off`,
    default: false,
    describe: `Exclude 'defer' attribute from tags`,
    type: `boolean`
  },
  k: {
    alias: `check`,
    default: false,
    describe: `Check if orpin can be run`,
    type: `boolean`
  },
  f: {
    alias: `force`,
    default: true,
    describe: `Fetch remote files`,
    type: `boolean`
  }
};
