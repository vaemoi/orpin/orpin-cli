const
  cheerio = require(`cheerio`),
  fs = require(`fs`),
  Joi = require(`@hapi/joi`),
  path = require(`path`),
  semver = require(`semver`);

const
  OrpinSchema = require(`./orpin-schema.js`),
  OrpinError = require(`./orpin-errors.js`);

/**
 * Make sure all needed data is read from the provided .orpinrc
 *
 * @param      {string}  configPath  Where to find the config file
 *
 * @return     {Object}  The path to the config file as an absolute path along with the parsed config file
 */
const checkConfig = (configPath) => {
  const result = { errors: [], value: {} };

  const basePath = path.resolve(fs.statSync(configPath).isDirectory() ?
    configPath : path.dirname(configPath)
  );

  const cfg = Joi.validate(
    JSON.parse(fs.readFileSync(path.join(basePath, `.orpinrc`))),
    OrpinSchema,
    { allowUnknown: true, stripUnknown: true }
  );

  if (cfg.error) {
    result.errors.push(cfg.error);

    return result;
  }

  result.value = { basePath, config: cfg.value };

  return result;
};

/**
 * Ensure that the values for the provided config file are valid
 *
 * @param      {string}   basePath  Where to find the configuration file
 * @param      {Object}   config    The parse config file
 *
 * @return     {boolean}  Data needed at runtime
 */
const testConfig = (basePath, config) => {
  const result = { errors: [], value: {} };

  const packageJSONPath = path.join(basePath, `package.json`);

  if (!fs.existsSync(packageJSONPath)) {
    result.errors.push(new OrpinError.Read(`package.json`, packageJSONPath));
  }

  const vendorPath = path.resolve(basePath, config.output.path);

  try {
    fs.accessSync(vendorPath, fs.constants.R_OK | fs.constants.W_OK);

    if (!fs.existsSync(vendorPath)) {
      fs.mkdirSync(vendorPath, {recursive: true});
    }
  } catch (err) {
    result.errors.push(new OrpinError.NoAccess(`vendorPath`, vendorPath));
  }

  const indexPath = path.resolve(basePath, config.indexFile);

  if (!fs.existsSync(indexPath)) {
    result.errors.push(new OrpinError.Read(path.basename(indexPath), indexPath));

    return result;
  }

  const indexFileDOM = cheerio.load(fs.readFileSync(indexPath));

  if (indexFileDOM.root().find(config.anchorSelector).length === 0) {
    result.errors.push(new OrpinError.Validation(`anchorSelector not found within indexFile DOM`));
  }

  result.value = { indexFileDOM, indexPath, packageJSONPath, vendorPath };

  return result;
};

/**
 * The base set of configs to start orpin
 *
 * @param      {string}  configPath  Where to load the .orpinrc file from
 *
 * @return     {Object}  Validated paths & flags from read from the configPath
 */
const meta = (configPath) => {
  const result = {
    errors: [],
    value: {
      config: null,
      indexFileDOM: null,
      indexPath: null,
      packageJSONPath: null,
      vendorPath: null
    }
  };

  if (!fs.existsSync(configPath)) {
    result.errors.push(new OrpinError.Read(`.orpinrc`, configPath));

    return result;
  }

  const { errors: chkErrors, value: { basePath, config } } = checkConfig(configPath);

  if (chkErrors) {
    result.errors.concat(chkErrors);

    return result;
  }

  const {
    errors: tstErrors,
    value: {
      indexFileDOM,
      indexPath,
      packageJSONPath,
      vendorPath
    }
  } = testConfig(basePath, config);

  if (tstErrors) {
    result.errors.concat(tstErrors);
  }

  result.value = {config, indexFileDOM, indexPath, packageJSONPath, vendorPath};

  return result;
};

/**
 * Checks a packge.json file for packages that match up with those in .orpinrc
 *     and joins the two data sources
 *
 * @param      {Object}   config           Parse .orpinrc
 * @param      {boolean}  devMode          Uuse local urls when injecting
 * @param      {Array}    packageVersions  Package names keyed to version numbers
 * @param      {string}   vendorPath       Where to reference packages files from
 *
 * @return     {Array}   package infos
 */
const buildPackages = (config, devMode, packageVersions = {}, vendorPath = ``) => {
  const result = { value: [], errors: [] };

  const validPackages = config.packages.filter((pkg) => {
    return !pkg.skip;
  });

  const baseURL = new URL(`/npm`, `https://cdn.jsdelivr.net`);

  result.value = validPackages.reduce((final, pkg) => {
    const { devPath, name: pkgName, path: prodPath } = pkg;

    let pkgVersion = packageVersions[pkgName];

    if (!pkgVersion) {
      result.errors.push(new OrpinError.MissingPackageVersion(pkgName));
    }

    pkgVersion = semver.valid(semver.coerce(packageVersions[pkgName]));

    let pkgPath = devMode && devPath ? devPath : prodPath;

    pkgPath = `${pkgName}@${pkgVersion}/${pkgPath}`;

    // :blob added later when fetching
    final.push({
      name: pkgName,
      version: pkgVersion,
      cdnURI: `${baseURL}/${pkgPath}`,
      localURI: path.resolve(vendorPath, pkgPath),
      srcURI: devMode ? `${config.output.srcPrefix}/${pkgPath}` : `${baseURL}/${pkgPath}`
    });

    return final;
  }, []);

  return result;
};

/**
 * Startup Orpin
 *
 * @param      {string}   maybeConfigPath  Where to find .orpinrc
 * @param      {boolean}  devMode          Flag to determine the url that's to be injected for each package
 *
 * @return     {Object}   Necessary data to run orpin
 */
const bootstrap = (maybeConfigPath, devMode) => {
  const result = { value: null, errors: [] };

  const configPath = maybeConfigPath ? maybeConfigPath : `.orpinrc`;

  const { errors: metaErrors, value: metaValue } = meta(configPath);

  if (metaErrors) {
    result.errors.concat(metaErrors);

    return result;
  }

  const { config, indexFileDOM, indexPath, packageJSONPath, vendorPath } = metaValue;

  const packageJSON = require(packageJSONPath);

  const {
    errors: pkgErrors,
    value: pkgs,
  } = buildPackages(config, devMode, packageJSON.dependencies, vendorPath);

  result.errors.concat(pkgErrors);

  result.value = {
    anchor: config.anchorSelector,
    indexDOM: indexFileDOM,
    indexPath: indexPath,
    pkgs: pkgs.value
  };

  return result;
};

module.exports = { go: bootstrap, meta, packages: buildPackages };
