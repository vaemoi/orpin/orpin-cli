# [orpin-cli](https://orpin.vaemoi.xyz)  [![npm version](https://badge.fury.io/js/%40vaemoi%2Forpin-cli.svg)](https://badge.fury.io/js/%40vaemoi%2Forpin-cli)
[![Known Vulnerabilities](https://snyk.io/test/npm/@vaemoi/orpin-cli/badge.svg)](https://snyk.io/test/npm/@vaemoi/orpin-cli)
[![Maintainability](https://api.codeclimate.com/v1/badges/875c50ede3d90f4714f7/maintainability)](https://codeclimate.com/repos/5d4fa9d6e3713f009d00045b/maintainability)
[![Coverage Status](https://coveralls.io/repos/gitlab/vaemoi/orpin/orpin-cli/badge.svg?branch=master)](https://coveralls.io/gitlab/vaemoi/orpin/orpin-cli?branch=master)

The cli interface for orpin

## About
orpin helps to manage npm packages in your web (read: browser) projects by keeping the `<script>` tags within your `<head></head>` element in sync with the versions in your package.json file. Currently orpin only supports jsdelivr for fetching any packages and as the url in the `src` attribute for the injected script tag.

## Configuration
orpin can be configured either via a config file `.orpinrc` or from flags passed to the cli.

orpin also depends on a file called `package.json` to know which version of files to get from the cdn. The `package.json` file should be located in the same directory as `.orpinrc`. The `pacakge.json` should have a key `dependencies` which is an object whose keys match the keys in `packages` within `.orpinrc` and the value is the semver compatible version number to use.

### `.orpinrc`
The following are the accepted values for an `.orpinrc.

#### anchorSelector
A valid dom selector to some element in the `indexFile` which tells orpin where to inject the script tags for the `packages`. orpin always injects before the `anchorSelector`. [cheerio](https://cheerio.js.org/) is used for querying and manipulating the dom.

#### indexFile
A path to an html file where the script tags for the `packages` will be injected.

#### packages
Each packages represents an npm package to download from cdnjs. Each package can specify three options `path` and `devPath` is where to find the file you want from the cdn; the url looks like:

`https://cdn.jsdelivr.net/npm/package@version/file`

The `devPath` takes precedency over `path` when it has been supplied a value and the environment variable `PRODUCTION` is not set. This is useful for when npm packges have production and development versions you would like to use (e.g React):

```javascript
{
    "packages": {
        "devPath": "umd/react.development.js",
        "path": "umd/react.production.js"
    }
}
```

#### vendorPath
A path to a directory where downloaded files will saved.

orpin will create the `vendorPath` if is does not already exist but will throw an error if the path exists and the current user does not have read and write permissions

#### vendorSrcPrefix
A string representing what the `src` attribute will be for the script tags injected into `indexFile`

---


A typical `.orpinrc` looks like: 
```javascript
{
  "anchorSelector": "script[src='js/something.js']",
  "indexFile": "public/index.html",
  "vendorPath": "public/assets/vendor",
  "vendorSrcPrefix": "assets/vendor",
  "packages": {
    "dompurify": {
      "path": "dist/purify.js"
    },
    "localforage": {
      "devPath": "dist/localforage.js",
      "path": "dist/localforage.min.js"
    },
    "lodash.debounce": {
      "skip": true
    }
  }
}
```

### CLI Parameters
The cli help page list all parameters and can be accessed with the -h flag.

```
➜ ./bin/cli.js --help

Usage:

    cli.js [<options>]

Options:
  -c, --config_path  Path to .orpinrc
  -d, --defer_off    Exclude 'defer' attribute from tags
  -k, --check        Check if orpin can be run
  -f, --force        Fetch remote files
  -h, --help         Show help
  -v, --version      Show version number

for more information, checkout https://gitlab.com/vaemoi/orpin/orpin-cli
```

## Examples
### cli usage
- Normal
```bash
orpin
```

- Passing the path to a config file
```bash
orpin -c ../some/path/to/.orpinrc
```

- Turning off defer and forcing download
```bash
orpin -d --force
```

- Prodcution (fetching files and injecting but not saving the file)
```bash
export PRODUCTION=true && orpin
```

### usage in NodeJS
(Coming soon)
